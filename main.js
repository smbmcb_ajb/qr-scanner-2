const fileinput = document.getElementById('qr-input-file');
const html5QrCode = new Html5Qrcode("reader", { fps: 10, qrbox: 250,aspectRatio: 2 });

        let scanQR = () =>
        {
            
            const qrCodeSuccessCallback = (decodedText, decodedResult) => 
            {
                /* handle success */
                // console.log(decodedText)
                // var result = decodedText.split(";").map(data => data)
                // document.getElementById("resultArea").innerHTML = `Part Number: ${result[6]}`
                document.getElementById("resultArea").innerHTML = decodedText
                console.log(decodedResult)
                stopScan()
            };
            const config = { fps: 10, qrbox: { width: 250, height: 250 } };


            // If you want to prefer back camera
            html5QrCode.start({ facingMode: "environment" }, config, qrCodeSuccessCallback);

            // document.getElementById("scanBtn").style.display = "none"
            // document.getElementById("stopScanBtn").style.display = "flex"

        }

        let stopScan = () =>
        {
            html5QrCode.stop().then((ignore) => {
            // QR Code scanning is stopped.
            }).catch((err) => {
            // Stop failed, handle it.
            });
            document.getElementById("scanBtn").style.display = "flex"
            document.getElementById("stopScanBtn").style.display = "none"
        }

        
        // File based scanning
        
        fileinput.addEventListener('change', e => 
        {
            if (e.target.files.length == 0) 
            {
            // No file selected, ignore 
                return;
            }
            const imageFile = e.target.files[0];
            // Scan QR Code
            html5QrCode.scanFile(imageFile, true)
            .then(decodedText => 
            {
                // success, use decodedText
                console.log(decodedText);
                var result = decodedText.split(";").map(data => data)
                console.log(result)
                document.getElementById("resultArea").innerHTML = `Part Number: ${result[6]}`
            })
            .catch(err => 
            {
                // failure, handle it.
                console.log(`Error scanning file. Reason: ${err}`)
                document.getElementById("resultArea").innerHTML = err
            });
        });
